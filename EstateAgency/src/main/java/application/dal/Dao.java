package application.dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class Dao {
	private static String url;
	private static String user;
	private static String password;

	protected static final String logTable = "logs";
	protected static final String credentialsTable = "credentials";
	protected static final String completedTransactionsTable = "completed_transactions";
	protected static final String stagingAptTable = "apts_staged";
	protected static final String transactionAptTable = "apts_in_transaction";

	protected Dao() {
		try {
			Context context = new InitialContext();
			url = (String) context.lookup("java:comp/env/db_url");
			user = (String) context.lookup("java:comp/env/db_user");
			password = (String) context.lookup("java:comp/env/db_password");
		} catch (NamingException e) {
			throw new RuntimeException("enable to get envuronment variable");
		}

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException ex) {
			throw new RuntimeException("enable to find the driver", ex);
		}
	}

	protected Connection getConnection() {
		try {
			return DriverManager.getConnection(url, user, password);
		} catch (SQLException ex) {
			throw new RuntimeException("data base connection failure", ex);
		}
	}
}
