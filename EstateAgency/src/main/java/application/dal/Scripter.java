package application.dal;

import application.domain.Apartment;
import application.domain.Furnishings;
import application.domain.Metric;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Scripter extends Dao {

	public Scripter() {
	}

	public Apartment getApartmentByPhone(String phone) {
		try {
			Connection connection = getConnection();
			PreparedStatement statement = connection
					.prepareStatement("select * from " + stagingAptTable + " where contact_phone = ?");
			statement.setString(1, phone);
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			Apartment apartment = extractApartment(resultSet);
			resultSet.close();
			statement.close();
			connection.close();
			return apartment;
		} catch (SQLException ex) {
			throw new RuntimeException("enable to get an apartment by such phone " + ex.getMessage());
		}
	}

	public void addApartment(Apartment apartment) {
		try {
			Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement("insert into " + stagingAptTable
					+ " (price, room_number, address, living_space, total_area, storey, furnishings, contact_phone, verified)"
					+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
			statement.setInt(1, apartment.getPrice());
			statement.setInt(2, apartment.getRoomNumber());
			statement.setString(3, apartment.getAddress());
			statement.setFloat(4, apartment.getLivingSpace());
			statement.setFloat(5, apartment.getTotalArea());
			statement.setInt(6, apartment.getStorey());
			statement.setString(7, apartment.getFurnishings().toString());
			statement.setString(8, apartment.getPhone());
			statement.setBoolean(9, false);
			statement.execute();
			statement.close();
			connection.close();
		} catch (SQLException ex) {
			throw new RuntimeException("enable to add new apartment", ex);
		}
	}

	public void updateBySample(Apartment sample) {
		try {
			Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement("update " + stagingAptTable
					+ " set price = ?, total_area = ?, living_space = ?, room_number = ?, storey = ?, address = ?, furnishings = ?"
					+ " where contact_phone = ?");
			statement.setInt(1, sample.getPrice());
			statement.setDouble(2, sample.getTotalArea());
			statement.setDouble(3, sample.getLivingSpace());
			statement.setInt(4, sample.getRoomNumber());
			statement.setInt(5, sample.getStorey());
			statement.setString(6, sample.getAddress());
			statement.setString(7, sample.getFurnishings().toString());
			statement.setString(8, sample.getPhone());
			statement.execute();
			statement.close();
			connection.close();
		} catch (SQLException ex) {
			throw new RuntimeException("enable to udpate on phone");
		}
	}

	public void deleteApartmentByPhone(String phone) {
		try {
			Connection connection = getConnection();
			PreparedStatement statementForStaging = connection
					.prepareStatement("delete from " + stagingAptTable + " where contact_phone = ?");
			statementForStaging.setString(1, phone);
			statementForStaging.execute();
			statementForStaging.close();
			PreparedStatement statementForTransaction = connection
					.prepareStatement("delete from " + transactionAptTable + " where contact_phone = ?");
			statementForTransaction.setString(1, phone);
			statementForTransaction.execute();
			statementForTransaction.close();
			connection.close();
		} catch (SQLException ex) {
			throw new RuntimeException("enable to delete apartment by phone", ex);
		}
	}

	public void verifyApartmentWithPhone(String phone) {
		try {
			Connection connection = this.getConnection();
			PreparedStatement statement = connection
					.prepareStatement("update " + stagingAptTable + " set verified = true where contact_phone = ?");
			statement.setString(1, phone);
			statement.execute();
			statement.close();
			connection.close();
		} catch (SQLException ex) {
			throw new RuntimeException("enable to set apartment as verified", ex);
		}
	}

	public void pushToTransaction(String phone) {
		try {
			Connection connection = getConnection();
			PreparedStatement copyToTransactions = connection.prepareStatement("insert into " + transactionAptTable
					+ " (price, total_area, living_space, room_number, storey, furnishings, address, contact_phone) "
					+ " select price, total_area, living_space, room_number, storey, furnishings, address, contact_phone from "
					+ stagingAptTable + " where contact_phone = ?");
			copyToTransactions.setString(1, phone);
			copyToTransactions.execute();
			copyToTransactions.close();
			PreparedStatement eraseFromStaging = connection
					.prepareStatement("delete from " + stagingAptTable + " where contact_phone = ?");
			eraseFromStaging.setString(1, phone);
			eraseFromStaging.execute();
			eraseFromStaging.close();
			connection.close();
		} catch (SQLException ex) {
			throw new RuntimeException("enable to push transaction", ex);
		}
	}

	public void takeBackToStaging(String phone) {
		try {
			Connection connection = getConnection();
			PreparedStatement copyToStaging = connection.prepareStatement("insert" + " into " + stagingAptTable
					+ " (price, total_area, living_space, room_number, storey, furnishings, address, contact_phone)"
					+ " select price, total_area, living_space, room_number, storey, furnishings, address, contact_phone"
					+ " from " + transactionAptTable + " where contact_phone = ?");
			copyToStaging.setString(1, phone);
			copyToStaging.execute();
			copyToStaging.close();
			PreparedStatement setAsVerified = connection.prepareStatement(
					"update " + stagingAptTable + " set verified = true" + " where contact_phone = ?");
			setAsVerified.setString(1, phone);
			setAsVerified.execute();
			setAsVerified.close();
			PreparedStatement eraseFromTransactions = connection
					.prepareStatement("delete from  " + transactionAptTable + " where contact_phone = ?");
			eraseFromTransactions.setString(1, phone);
			eraseFromTransactions.execute();
			eraseFromTransactions.close();

			connection.close();
		} catch (SQLException ex) {
			throw new RuntimeException("enable to take apartment back to staging", ex);
		}
	}

	public List<String> getAllPhones() {
		try {
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("select " + stagingAptTable + ".contact_phone from "
					+ stagingAptTable + " full join " + transactionAptTable + " on true");
			List<String> phones = new ArrayList<>();
			while (resultSet.next()) {
				phones.add(resultSet.getString("contact_phone"));
			}
			resultSet.close();
			statement.close();
			connection.close();
			return phones;
		} catch (SQLException ex) {
			throw new RuntimeException("enable to get phones", ex);
		}
	}

	public List<Apartment> getApartmentsByMetric(Metric metric, String orderBy) {
		try {
			Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement("select * from " + stagingAptTable
					+ " where (price <= ? and price >= ?) " + "and (total_area <= ? and total_area >= ?) "
					+ "and room_number = ? and furnishings = ? " + "and verified = true " + "order by " + orderBy);
			statement.setInt(1, metric.getMaxPrice());
			statement.setInt(2, metric.getMinPrice());
			statement.setFloat(3, metric.getMaxArea());
			statement.setFloat(4, metric.getMinArea());
			statement.setInt(5, metric.getRoomNumber());
			statement.setString(6, metric.getFurnishings().toString());
			ResultSet resultSet = statement.executeQuery();
			List<Apartment> apartments = new ArrayList<>();
			while (resultSet.next()) {
				apartments.add(this.extractApartment(resultSet));
			}
			resultSet.close();
			statement.close();
			connection.close();
			return apartments;
		} catch (SQLException ex) {
			throw new RuntimeException("no apartment with such metric is registered", ex);
		}
	}

	public List<Apartment> getApartmentsByVerification(boolean isVerified) {
		try {
			Connection connection = this.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("select * from " + stagingAptTable + " where verified = " + isVerified);
			List<Apartment> apartments = new ArrayList<>();
			while (resultSet.next()) {
				apartments.add(this.extractApartment(resultSet));
			}
			resultSet.close();
			statement.close();
			connection.close();
			return apartments;
		} catch (SQLException ex) {
			throw new RuntimeException("enable to get apartments by verification status", ex);
		}
	}

	public List<Apartment> getApartmentsInTransaction() {
		try {
			Connection connection = this.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from " + transactionAptTable);
			List<Apartment> apartments = new ArrayList<>();
			while (resultSet.next()) {
				apartments.add(this.extractApartment(resultSet));
			}
			resultSet.close();
			statement.close();
			connection.close();
			return apartments;
		} catch (SQLException ex) {
			throw new RuntimeException("enable to get apartments by verification status", ex);
		}
	}

	private Apartment extractApartment(ResultSet resultSet) {
		try {
			Apartment apartment = new Apartment();
			apartment.setPrice(resultSet.getInt("price"));
			apartment.setRoomNumber(resultSet.getInt("room_number"));
			apartment.setAddress(resultSet.getString("address"));
			apartment.setLivingSpace(resultSet.getFloat("living_space"));
			apartment.setTotalArea(resultSet.getFloat("total_area"));
			apartment.setStorey(resultSet.getInt("storey"));
			String furnishingsType = resultSet.getString("furnishings");
			switch (furnishingsType) {
			case "NONE":
				apartment.setFurnishings(Furnishings.NONE);
				break;
			case "PARTIAL":
				apartment.setFurnishings(Furnishings.PARTIAL);
				break;
			case "FULL":
				apartment.setFurnishings(Furnishings.FULL);
				break;
			default:
				throw new RuntimeException(furnishingsType + Furnishings.NONE.toString() + "|");
			}
			apartment.setPhone(resultSet.getString("contact_phone"));
			return apartment;
		} catch (SQLException ex) {
			throw new RuntimeException("enable to extract apartment", ex);
		}
	}
}
