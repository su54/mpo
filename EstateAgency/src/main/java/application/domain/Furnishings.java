package application.domain;

public enum Furnishings {
    NONE,
    FULL,
    PARTIAL
}
