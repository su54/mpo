package application.domain;

public class Metric {
    private int minPrice;
    private int maxPrice;
    private float minArea;
    private float maxArea;
    private int roomNumber;
    private Furnishings furnishings;

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
    }

    public float getMinArea() {
        return minArea;
    }

    public void setMinArea(float minArea) {
        this.minArea = minArea;
    }

    public float getMaxArea() {
        return maxArea;
    }

    public void setMaxArea(float maxArea) {
        this.maxArea = maxArea;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Furnishings getFurnishings() {
        return furnishings;
    }

    public void setFurnishings(Furnishings furnishings) {
        this.furnishings = furnishings;
    }
}
