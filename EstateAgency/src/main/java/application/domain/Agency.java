package application.domain;

import application.dal.Logger;
import application.dal.Scripter;

import java.util.ArrayList;
import java.util.List;

public class Agency {
	private static Agency instance = null;
	private Logger logger;
	private Scripter scripter;

	private Agency() {
		logger = new Logger();
		scripter = new Scripter();
	}

	public static Agency getInstance() {
		if (instance == null) {
			instance = new Agency();
		}
		return instance;
	}

	public void completeTransactionOnPhone(String phone) {
		logger.recordTransactionOnPhone(phone);
	}

	public void updateBySample(Apartment sample) {
		List<String> validationList = getValidationList(sample);
		String issue = "";
		if (!scripter.getAllPhones().contains(sample.getPhone())) {
			issue += " there is no such phone";
		}
		if (!validationList.isEmpty()) {
			issue += validationList.toString();
		}
		if (!issue.equals("")) {
			throw new RuntimeException(issue);
		} else {
			Apartment aptToUpdate = scripter.getApartmentByPhone(sample.getPhone());
			mergeIntoSample(aptToUpdate, sample);
			scripter.updateBySample(sample);
		}
	}

	public void addApartment(Apartment apartment) {
		List<String> validationList = getValidationList(apartment);
		String issue = "";
		if (scripter.getAllPhones().contains(apartment.getPhone())) {
			issue = "such phone number already registered";
		}
		if (!validationList.isEmpty()) {
			issue = validationList.toString();
		}
		if (issue.isEmpty()) {
			scripter.addApartment(apartment);
		} else {
			throw new RuntimeException(issue);
		}
	}

	public List<Apartment> getApartmentsByMetric(Metric metric, String orderBy) {
		List<String> validationList = getValidationLIst(metric);
		String issue = "";
		if (validationList.isEmpty()) {
			try {
				return scripter.getApartmentsByMetric(metric, orderBy);
			} catch (RuntimeException ex) {
				issue = ex.getCause().getMessage();
			}
		} else {
			issue = validationList.toString();
		}
		throw new RuntimeException(issue);
	}

	public void registerEntering() {
		logger.addLog();
	}

	public boolean doesAccept(Role role) {
		return logger.doesAccept(role);
	}

	public void pushToTransaction(String phone) {
		scripter.pushToTransaction(phone);
	}

	public void cancelTransaction(String phone) {
		scripter.takeBackToStaging(phone);
	}

	public void verifyApartmentWithPhone(String phone) {
		scripter.verifyApartmentWithPhone(phone);
	}

	public void deleteApartmentByPhone(String phone) {
		scripter.deleteApartmentByPhone(phone);
	}

	public List<Apartment> getApartmentsToVerify() {
		return scripter.getApartmentsByVerification(false);
	}

	public List<Apartment> getVerifiedApartments() {
		return scripter.getApartmentsByVerification(true);
	}

	public List<Apartment> getApartmentsInTransaction() {
		return scripter.getApartmentsInTransaction();
	}

	public boolean isContactPhoneValid(String contactPhone) {
		String phone = contactPhone;
		phone = phone.replace("+", "");
		phone = phone.replace(" ", "");
		if (phone.matches(".*[a-zA-Z]+.*")) {
			return false;
		}
		return true;
	}

	private void mergeIntoSample(Apartment aptToUpdate, Apartment sample) {
		if (sample.getPrice() == 1) {
			sample.setPrice(aptToUpdate.getPrice());
		}
		if (sample.getTotalArea() == 1) {
			sample.setTotalArea(aptToUpdate.getTotalArea());
		}
		if (sample.getLivingSpace() == 1) {
			sample.setLivingSpace(aptToUpdate.getLivingSpace());
		}
		if (sample.getRoomNumber() == 1) {
			sample.setRoomNumber(aptToUpdate.getRoomNumber());
		}
		if (sample.getStorey() == 1) {
			sample.setStorey(aptToUpdate.getStorey());
		}
		if (sample.getAddress().equals("1")) {
			sample.setAddress(aptToUpdate.getAddress());
		}
	}

	private List<String> getValidationList(Apartment apartment) {
		List<String> validationList = new ArrayList<>();
		if (!isContactPhoneValid(apartment.getPhone())) {
			validationList.add("invalid phone number format");
		}
		if (apartment.getPrice() <= 0) {
			validationList.add("the price shall be positive");
		}
		if (apartment.getTotalArea() <= 0) {
			validationList.add("the total area shall be positive");
		}
		if (apartment.getLivingSpace() <= 0) {
			validationList.add("the living space shall be positive");
		}
		if (apartment.getRoomNumber() <= 0) {
			validationList.add("the room number shall be positive");
		}
		if (apartment.getStorey() <= 0) {
			validationList.add("the storey shall be positive");
		}
		if ((int) apartment.getLivingSpace() > (int) apartment.getTotalArea()) {
			validationList.add("impossible ratio of total area and living space");
		}
		return validationList;
	}

	private List<String> getValidationLIst(Metric metric) {
		List<String> validationList = new ArrayList<>();
		if (metric.getMaxPrice() <= 0) {
			validationList.add("max price shall be positive");
		}
		if (metric.getMinPrice() < 0) {
			validationList.add("min price shall be positive");
		}
		if (metric.getMaxArea() <= 0) {
			validationList.add("max area shall be positive");
		}
		if (metric.getMinArea() < 0) {
			validationList.add("min area shall be positive");
		}
		if (metric.getRoomNumber() <= 0) {
			validationList.add("room number shall be positive");
		}
		if (metric.getMaxPrice() < metric.getMinPrice()) {
			validationList.add("invalid ratio of max and min prices");
		}
		if (metric.getMaxArea() < metric.getMinArea()) {
			validationList.add("invalid ratio of max and min areas");
		}
		return validationList;
	}

}
