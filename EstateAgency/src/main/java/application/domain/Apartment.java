package application.domain;

public class Apartment {
    private int price;
    private int roomNumber;
    private String address;
    private float totalArea;
    private float livingSpace;
    private int storey;
    private Furnishings furnishings;
    private String phone;

    @Override
    public String toString() {
        return this.toPublicString() + "; contact phone: " + this.phone;
    }
    
    public String toPublicString() {
    	return this.price + " GPB; " + this.roomNumber + " rooms; " + "furnishings type: " + this.furnishings.toString() + ";" + '\n'
                + '\t' + "living space: " + this.livingSpace + " sq. m; " + "in total: " + this.totalArea + " sq. m; " + '\n'
                + '\t' + "address: " + this.address + "; " + this.storey +" floor";
    }

    public Furnishings getFurnishings() {
        return furnishings;
    }

    public void setFurnishings(Furnishings furnishings) {
        this.furnishings = furnishings;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getTotalArea() {
        return totalArea;
    }

    public void setTotalArea(float totalArea) {
        this.totalArea = totalArea;
    }

    public float getLivingSpace() {
        return livingSpace;
    }

    public void setLivingSpace(float livingSpace) {
        this.livingSpace = livingSpace;
    }

    public int getStorey() {
        return storey;
    }

    public void setStorey(int storey) {
        this.storey = storey;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
