package application.web;

import application.domain.Agency;
import application.domain.Apartment;
import application.domain.Furnishings;
import application.domain.Metric;

import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SearchingServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("WEB-INF/pages/searching.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Metric metric = new Metric();
        List<Apartment> apartmentList = new ArrayList<>();
        List<String> issueList = new ArrayList<>();
        try {
            metric.setMinPrice(Integer.parseInt(request.getParameter("minPrice")));
        } catch (NumberFormatException ex) {
            issueList.add("invalid min price input");
        }
        try {
            metric.setMaxPrice(Integer.parseInt(request.getParameter("maxPrice")));
        } catch (NumberFormatException ex) {
            issueList.add("invalid max price input");
        }
        try {
            metric.setMinArea(Float.parseFloat(request.getParameter("minTotalArea")));
        } catch (Exception ex) {
            issueList.add("invalid min living space input");
        }
        try {
            metric.setMaxArea(Float.parseFloat(request.getParameter("maxTotalArea")));
        } catch (Exception ex) {
            issueList.add("invalid max living space input");
        }
        try {
            metric.setRoomNumber(Integer.parseInt(request.getParameter("roomNumber")));
        } catch (NumberFormatException ex) {
            issueList.add("invalid room number input");
        }
        switch (request.getParameter("furnishingsType")) {
            case "none":
                metric.setFurnishings(Furnishings.NONE);
                break;
            case "partial":
                metric.setFurnishings(Furnishings.PARTIAL);
                break;
            case "full":
                metric.setFurnishings(Furnishings.FULL);
                break;
        }
        String orderBy = request.getParameter("sortOrder");
        if (orderBy.equals("area")) {
        	orderBy = "total_area";
        }
        if (issueList.isEmpty()) {
            try {
                Agency agency = Agency.getInstance();
                apartmentList = agency.getApartmentsByMetric(metric, orderBy);
            } catch (RuntimeException ex) {
                issueList.add(ex.getMessage());
            }
            if (issueList.isEmpty()) {
                request.setAttribute("apartmentList", apartmentList);
                request.getRequestDispatcher("WEB-INF/pages/foundlings.jsp").forward(request, response);
            }
            else {
                request.setAttribute("issueList", issueList);
                request.getRequestDispatcher("WEB-INF/pages/searching.jsp").forward(request, response);
            }
        }
        else {
            request.setAttribute("issueList", issueList);
            request.getRequestDispatcher("WEB-INF/pages/searching.jsp").forward(request, response);
        }
    }
}
