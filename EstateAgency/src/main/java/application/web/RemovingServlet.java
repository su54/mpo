package application.web;

import application.dal.Scripter;
import application.domain.Agency;
import application.domain.Apartment;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class RemovingServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] phonesToRemove = request.getParameterValues("phones");
        Agency agency = Agency.getInstance();
        if (phonesToRemove != null) {
            for (String phone : phonesToRemove) {
                agency.deleteApartmentByPhone(phone);
            }
        }
        request.setAttribute("apartmentsToVerify", agency.getApartmentsToVerify());
        request.setAttribute("verifiedApartments", agency.getVerifiedApartments());
        request.setAttribute("apartmentsInTransaction", agency.getApartmentsInTransaction());
        request.getRequestDispatcher("WEB-INF/pages/registration.jsp").forward(request, response);
    }
}
