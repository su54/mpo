package application.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import application.domain.Agency;

public class CompletingServlet extends HttpServlet {
	 @Override
	    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        String[] phonesOfTransactions = request.getParameterValues("phones");
	        Agency agency = Agency.getInstance();
	        if (phonesOfTransactions != null) {
	            for (String phone : phonesOfTransactions) {
	                agency.completeTransactionOnPhone(phone);
	            }
	        }
	        request.setAttribute("apartmentsToVerify", agency.getApartmentsToVerify());
	        request.setAttribute("verifiedApartments", agency.getVerifiedApartments());
	        request.setAttribute("apartmentsInTransaction", agency.getApartmentsInTransaction());
	        request.getRequestDispatcher("WEB-INF/pages/registration.jsp").forward(request, response);
	    }

}
