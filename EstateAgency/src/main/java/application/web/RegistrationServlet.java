package application.web;

import application.domain.Agency;
import application.domain.Apartment;
import application.domain.Role;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class RegistrationServlet extends HomeServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        Role role = new Role(login, password);
        Agency agency = Agency.getInstance();
        if (agency.doesAccept(role)) {
            request.setAttribute("apartmentsToVerify", agency.getApartmentsToVerify());
            request.setAttribute("verifiedApartments", agency.getVerifiedApartments());
            request.setAttribute("apartmentsInTransaction", agency.getApartmentsInTransaction());
            request.getRequestDispatcher("WEB-INF/pages/registration.jsp").forward(request, response);
        }
        else {
            String issueList = "wrong login or password";
            request.setAttribute("issueList", issueList);
            request.getRequestDispatcher("WEB-INF/pages/home.jsp").forward(request, response);
        }
    }
}
