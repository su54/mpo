package application.web;

import application.domain.Agency;
import application.domain.Apartment;
import application.domain.Furnishings;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ApplicationServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("WEB-INF/pages/applicationForm.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Apartment apartment = new Apartment();
        List<String> issueList = new ArrayList<>();
        try {
            apartment.setPrice(Integer.parseInt(request.getParameter("price")));
        } catch (NumberFormatException ex) {
            issueList.add("invalid price input");
        }
        try {
            apartment.setTotalArea(Float.parseFloat(request.getParameter("totalArea")));
        } catch (NumberFormatException ex) {
            issueList.add("invalid total area input");
        }
        try {
            apartment.setLivingSpace(Float.parseFloat(request.getParameter("livingSpace")));
        } catch (NumberFormatException ex) {
            issueList.add("invalid living space input");
        }
        try {
            apartment.setRoomNumber(Integer.parseInt(request.getParameter("roomNumber")));
        } catch (NumberFormatException ex) {
            issueList.add("invalid room number input");
        }
        try {
            apartment.setStorey(Integer.parseInt(request.getParameter("storey")));
        } catch (NumberFormatException ex) {
            issueList.add("invalid storey input");
        }
        switch (request.getParameter("furnishingsType")) {
            case "none":
                apartment.setFurnishings(Furnishings.NONE);
                break;
            case "partial":
                apartment.setFurnishings(Furnishings.PARTIAL);
                break;
            case "full":
                apartment.setFurnishings(Furnishings.FULL);
                break;
        }
        String address = request.getParameter("address");
        if (address != null) {
            apartment.setAddress(address);
        }
        else {
            issueList.add("invalid address input");
        }
        String phone = request.getParameter("phoneNumber");
        if (phone != null) {
            apartment.setPhone(phone);
        }
        else {
            issueList.add("invalid phone input");
        }
        if (issueList.isEmpty()) {
            try {
                Agency agency = Agency.getInstance();
                agency.addApartment(apartment);
            } catch (RuntimeException ex) {
                issueList.add(ex.getMessage());
            }
            finally {
                request.setAttribute("issueList", issueList);
                request.getRequestDispatcher("WEB-INF/pages/applicationForm.jsp").forward(request, response);
            }
        } else {
            request.setAttribute("issueList", issueList);
            request.getRequestDispatcher("WEB-INF/pages/applicationForm.jsp").forward(request, response);
        }
    }
}
