<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
  <title>Foundlings</title>
  <style>
    body {
      background-color: aliceblue;
      font-family: calibri;
    }
    .button_submit {
      border-radius:5px;
      background-color:lightblue;
      width: 75px;
    }
    a:link {
      text-decoration: none;
    }
    a:hover {
      text-decoration: underline;
    }
  </style>
</head>
<body>
<h2 style="text-align:center; color:teal">Results</h2>
<h4 style="text-align:left; color:teal">Variants based on specified parameters:</h4>
<div>
  <c:if test="${apartmentList.isEmpty() == true}">
    <c:out value="Nothing found based on specified parameters"></c:out>
  </c:if>
  <c:if test="${apartmentList.isEmpty() == false}">
    <ul>
      <c:forEach var="apartment" items="${apartmentList}">
        <li>${apartment.toPublicString()}</li>
      </c:forEach>
    </ul>
  </c:if>
</div>

<div style="color: lightblue; text-align: center">
  <a href="${pageContext.request.contextPath}/" style="color: teal">To homepage</a>
   ||
  <a href="${pageContext.request.contextPath}/searching" style="color: teal">To searching</a>
   ||
  <a href="${pageContext.request.contextPath}application/" style="color: teal">To application form</a>
  <br/>
    <c style="color: teal">Our contact phone: +44 *** **** ***</c>
</div>
</body>
</html>